﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace UDP_Client
{
    class Client
    {
        public static byte[] receive_buffer;
        
        public enum DataType
        {
            SYN = 0, SYNACK = 1, ACK = 2, FIN = 3, FINACK = 4, DATA = 5 
        }

        struct Data     //struktur data yang akan dipakai
        {
            public int status;      //status yang akan digunakan(SYN, SYNACK)
            public int packet;      //inisialisasi paket
        };

        static void Main(string[] args)
        {

            //Initializing for socket and others
            var client = new UdpClient();
            Boolean done = false;
            Data data = new Data();
            DataType tipe = DataType.SYN;
            bool wait = false;
            Socket sending_socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp); //syntax untuk menandakan protocol UDP            
            IPAddress send_to_address = IPAddress.Parse("127.0.0.1"); // ip address yang digunakan = localhost
            IPEndPoint sending_end_point = new IPEndPoint(send_to_address, 11000);
            Console.WriteLine("Client connect to server...");

            client.Connect(sending_end_point);

            while (!done)
            {

                byte[] send_buffer = new byte [1024];

                if (tipe != DataType.DATA && !wait)
                {
                    if (tipe == DataType.SYN)
                    {
                        Console.WriteLine("Client send SYN...");
                        data.status = (int)DataType.SYN;
                    }

                    else if (tipe == DataType.ACK)
                    {
                        Console.WriteLine("Client send ACK...");
                        data.status = (int)DataType.ACK;
                        tipe = DataType.DATA;                        
                    }

                    data.packet = 0;

                    string temp = data.status + "-" + data.packet;
                    send_buffer = Encoding.ASCII.GetBytes(temp);

                    client.Send(send_buffer, send_buffer.Length);

                    wait = true;
                    
                    
                }

                var buffer = client.Receive(ref sending_end_point);
                string rcv = Encoding.ASCII.GetString(buffer);
                data.status = Convert.ToInt16(rcv.Substring(0, rcv.IndexOf("-")));
                data.packet = Convert.ToInt16(rcv.Substring(rcv.IndexOf("-") + 1));
                Console.WriteLine("Recv: stats {0} data {1}", data.status, data.packet);

                if (data.status == (int)DataType.SYN)
                {

                }

                else if (data.status == (int)DataType.SYNACK)
                {
                    tipe = DataType.ACK;

                    Console.WriteLine("Client got SYNACK!");
                    wait = false;
                }

                else if (data.status == (int)DataType.ACK)
                {

                }

                else if (data.status == (int)DataType.FIN)
                {

                }

                else if (data.status == (int)DataType.FINACK)
                {
                    Console.WriteLine("Client got FINACK!");
                    Console.WriteLine("Client send ACK...");

                    data.status = (int)DataType.ACK;
                    data.packet = 0;

                    string temp = data.status + "-" + data.packet;
                    var buf = Encoding.ASCII.GetBytes(temp);

                    client.Send(buf, buf.Length);

                    done = true;
                }

                else if (data.status == (int)DataType.DATA)
                {
                    Console.WriteLine("Client got DATA!");

                    if (data.packet < 5000)
                    {
                        data.status = (int)DataType.DATA;
                        data.packet = 10;

                        string temp = data.status + "-" + data.packet;
                        var buf = Encoding.ASCII.GetBytes(temp);

                        for (int i = 0; i < 5; i++)
                        {
                            //client.Send(send_buffer, send_buffer.Length);
                            client.Send(buf, buf.Length);
                        }

                    }

                    else if (data.packet == 5000)
                    {
                        Console.WriteLine("Client send FIN...");

                        data.packet = 0;
                        data.status = (int)DataType.FIN;

                        string temp = data.status + "-" + data.packet;
                        var buf = Encoding.ASCII.GetBytes(temp);

                        client.Send(buf, buf.Length);
                    }

                }

                //Console.WriteLine("Enter text to send, blank line to quit");

                //if (text_to_send.Length == 0)
                //{
                //    done = true;
                //}
                //else

                //{
                //    // the socket object must have an array of bytes to send.

                //    // this loads the string entered by the user into an array of bytes.




                //    // Remind the user of where this is going.

                //    Console.WriteLine("sending to address: {0} port: {1}",
                //    sending_end_point.Address,
                //    sending_end_point.Port);
                //    try

                //    {
                //        sending_socket.SendTo(send_buffer, sending_end_point);
                //        //byte[] receivedData;

                //        sending_socket.Receive(receive_buffer);
                //        String RE = Encoding.ASCII.GetString(receive_buffer);
                //        Console.WriteLine(RE);

                //    }
                //    catch (Exception send_exception)
                //    {
                //        exception_thrown = true;
                //        //Console.WriteLine(" Exception {0}", send_exception.Message);
                //    }

                //    if (exception_thrown == true)
                //    {
                //        Console.WriteLine("Message has been sent to the broadcast address");
                //    }

                //    else

                //    {
                //        exception_thrown = false;
                //        Console.WriteLine("The exception indicates the message was not sent.");
                //    }
                //}
            } // end of while (!done)

            Console.WriteLine("Closing Socket...");
            sending_socket.Shutdown(SocketShutdown.Both);
            sending_socket.Close();
        }
    }
}
